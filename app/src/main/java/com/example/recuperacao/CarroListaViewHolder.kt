package com.example.recuperacao

import android.view.View
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.recuperacao.model.Carro

class CarroListaViewHolder (
    itemView: View
) : RecyclerView.ViewHolder(itemView) {

    private val textoCategoria: TextView = itemView.findViewById(R.id.textoCategoria)
    private val textoValMin: TextView = itemView.findViewById(R.id.textoValMin)
    private val textoDataEntrada: TextView = itemView.findViewById(R.id.textoDataEntrada)
    private val textoDescricao: TextView = itemView.findViewById(R.id.textoDescricao)
    private val txtDescricao: TextView = itemView.findViewById(R.id.txtDescricao)
    private lateinit var carro: Carro

    init{
        this.txtDescricao.setOnClickListener{
            if(textoDescricao.visibility == View.GONE){
                textoDescricao.visibility = View.VISIBLE
                txtDescricao.setText(R.string.txt_descricao_retrair)
            }   else{
                textoDescricao.visibility = View.GONE
                txtDescricao.setText(R.string.txt_descricao_expandir)
            }
        }
    }

    fun bind(carro: Carro){
        this.carro = carro
        this.textoCategoria.text = carro.getCategory()
        this.textoValMin.text = "R$" + carro.getMinValue().toString()
        this.textoDataEntrada.text = carro.getEntryDate()
        this.textoDescricao.text = carro.getDescription()

    }
}