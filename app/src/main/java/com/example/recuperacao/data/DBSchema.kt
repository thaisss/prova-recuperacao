package com.example.recuperacao.data

object DBSchema {
    object Table{
        const val TABLENAME = "carros"
        const val ID = "c_id"
        const val CATEGORIA = "c_categoria"
        const val VAL_MIN = "c_valor_min"
        const val DATA_ENTRADA = "c_data_entrada"
        const val DESCRICAO = "c_descricao"

        fun getCreateTableQuery(): String{
            return """ 
                CREATE TABLE IF NOT EXISTS $TABLENAME (
                    $ID INTEGER PRIMARY KEY AUTOINCREMENT,
                    $CATEGORIA TEXT NOT NULL,
                    $VAL_MIN REAL NOT NULL,
                    $DATA_ENTRADA TEXT NOT NULL,
                    $DESCRICAO TEXT NOT NULL
                );
            """.trimIndent()
        }
    }
}