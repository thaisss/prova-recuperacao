package com.example.recuperacao.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.*
import com.example.recuperacao.R
import com.example.recuperacao.data.DBTableManager
import com.example.recuperacao.model.Carro
import java.time.LocalDate
import java.time.format.DateTimeFormatter

class AdicionarVeiculoActivity : AppCompatActivity() {

    private lateinit var spinner : Spinner
    private lateinit var caixaValorMinimo : EditText
    private lateinit var caixaDataEntrada : EditText
    private lateinit var caixaDescricao : EditText
    private lateinit var botaoHoje : Button
    private lateinit var botaoAdicionarVeiculo : Button
    private lateinit var dbCarros : DBTableManager

    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_adicionar_veiculo)
        val adapter = ArrayAdapter.createFromResource(this, R.array.arrayCategorias,android.R.layout.simple_spinner_item)
                            .also{adapter -> adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)}

        this.spinner = findViewById(R.id.spinner)
        this.spinner.adapter = adapter
        this.caixaValorMinimo = findViewById(R.id.caixaValorMinimo)
        this.caixaDescricao = findViewById(R.id.caixaDescricao)
        this.caixaDataEntrada = findViewById(R.id.caixaDataEntrada)
        this.botaoHoje = findViewById(R.id.botaoHoje)
        this.botaoAdicionarVeiculo = findViewById(R.id.botaoAdicionarVeiculo)
        this.dbCarros = DBTableManager(this)
    }

    fun onClickHoje(v: View){
        val current = LocalDate.now()
        val formatter = DateTimeFormatter.ofPattern("dd/MM/yyyy")
        val formatted = current.format(formatter)

        this.caixaDataEntrada.setText(formatted)

    }

    fun onClickAdicionar(v: View){

        var verificar : Boolean = true

        val categoria = this.spinner.selectedItem.toString()
        val valMin = this.caixaValorMinimo.text.toString()
        val dataEntrada = this.caixaDataEntrada.text.toString()
        val descricao = this.caixaDescricao.text.toString()

        if(valMin.isEmpty()){
            verificar = false
        }
        if(dataEntrada.isEmpty()){
            verificar = false
        }
        if(descricao.isEmpty()){
            verificar = false
        }

        if(verificar){
            val car = Carro(categoria, valMin.toDouble(),dataEntrada,descricao)
            dbCarros.inserir(car)

            Toast.makeText(this, R.string.txt_veiculo_adicionado,Toast.LENGTH_SHORT).show()
            finish()
        }   else{
            Toast.makeText(this, R.string.txt_erro_adicionar,Toast.LENGTH_SHORT).show()
        }

    }
}