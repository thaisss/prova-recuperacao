package com.example.recuperacao.model

class Carro(private val categoria: String, private val valMin: Double, private val dataEntrada: String, private val descricao: String) {
    private var id : Long = 0

    constructor(id : Long, categoria: String,valMin: Double,dataEntrada: String,descricao: String ): this(categoria, valMin, dataEntrada, descricao){
        this.id = id
    }

    fun getCategory(): String{
        return categoria
    }

    fun getMinValue(): Double{
        return valMin
    }

    fun getEntryDate(): String{
        return dataEntrada
    }

    fun getDescription(): String{
        return descricao
    }

    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as Carro

        if (id != other.id) return false

        return true
    }

    override fun hashCode(): Int {
        return id.hashCode()
    }


}