package com.example.recuperacao

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.recuperacao.model.Carro

class CarroListaAdapter(private val listaCarros : ArrayList<Carro>) : RecyclerView.Adapter<CarroListaViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CarroListaViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val itemView = inflater.inflate(R.layout.view_carro, parent, false)
        return CarroListaViewHolder(itemView)
    }

    override fun onBindViewHolder(holder: CarroListaViewHolder, position: Int) {
        holder.bind(this.listaCarros[position])
    }

    override fun getItemCount(): Int {
        return this.listaCarros.size
    }
}