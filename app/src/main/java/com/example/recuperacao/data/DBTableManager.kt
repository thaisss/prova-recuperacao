package com.example.recuperacao.data

import android.content.ContentValues
import android.content.Context
import com.example.recuperacao.model.Carro

class DBTableManager(context : Context) {
    companion object{
        val COLS = arrayOf(
            DBSchema.Table.ID,
            DBSchema.Table.CATEGORIA,
            DBSchema.Table.VAL_MIN,
            DBSchema.Table.DATA_ENTRADA,
            DBSchema.Table.DESCRICAO
        )
    }

    private val dbHelper = DBHelper(context)

    fun inserir(carro: Carro) {
        val cv = ContentValues()
        cv.put(DBSchema.Table.CATEGORIA, carro.getCategory())
        cv.put(DBSchema.Table.VAL_MIN, carro.getMinValue())
        cv.put(DBSchema.Table.DATA_ENTRADA, carro.getEntryDate())
        cv.put(DBSchema.Table.DESCRICAO, carro.getDescription())
        val db = this.dbHelper.writableDatabase
        db.insert(DBSchema.Table.TABLENAME, null, cv)
        db.close()
    }

    fun receberCarros(): ArrayList<Carro> {
        val carros : ArrayList<Carro> = ArrayList()
        val db = this.dbHelper.readableDatabase
        val cur = db.query(DBSchema.Table.TABLENAME, COLS, null, null, null, null, null)
        while(cur.moveToNext()){
            val id = cur.getLong(cur.getColumnIndexOrThrow(DBSchema.Table.ID))
            val category = cur.getString(cur.getColumnIndexOrThrow(DBSchema.Table.CATEGORIA))
            val minValue = cur.getDouble(cur.getColumnIndexOrThrow(DBSchema.Table.VAL_MIN))
            val entryDate = cur.getString(cur.getColumnIndexOrThrow(DBSchema.Table.DATA_ENTRADA))
            val description = cur.getString(cur.getColumnIndexOrThrow(DBSchema.Table.DESCRICAO))
            val carro = Carro(id,category,minValue,entryDate,description)
            carros.add(carro)
        }
        db.close()
        return carros
    }
}