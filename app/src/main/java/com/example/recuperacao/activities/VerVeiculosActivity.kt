package com.example.recuperacao.activities

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.recuperacao.CarroListaAdapter
import com.example.recuperacao.R
import com.example.recuperacao.data.DBTableManager

class VerVeiculosActivity : AppCompatActivity() {
    private lateinit var rvCarros : RecyclerView
    private lateinit var dbCarros : DBTableManager

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_ver_veiculos)

        this.rvCarros = findViewById(R.id.rvCarros)
        this.dbCarros = DBTableManager(this)
        this.rvCarros.layoutManager = LinearLayoutManager(this)
        this.rvCarros.adapter = CarroListaAdapter(dbCarros.receberCarros())
    }
}